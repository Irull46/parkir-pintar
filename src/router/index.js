import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { Splash, OpsiParkir, Home, TambahNopol, EditNopol } from '../pages';

const Stack = createStackNavigator();

const Router = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen
                name="Splash"
                component={Splash}
                options={{headerShown: false}}
            />
            <Stack.Screen 
                name="OpsiParkir"
                component={OpsiParkir}
                options={{headerShown: false}}
            />
            <Stack.Screen 
                name="Home"
                component={Home}
                options={{
                    title: 'Daftar Parkir',
                    headerStyle: {
                        backgroundColor: 'blue',
                        borderBottomLeftRadius: 12,
                        borderBottomRightRadius: 12
                    },
                    headerTintColor: 'white',
                    headerTitleStyle: { fontWeight: 'bold' }
                }}
            />
            <Stack.Screen
                name="TambahNopol"
                component={TambahNopol}
                options={{
                    title: 'Tambah No. Polisi',
                    headerStyle: {
                        backgroundColor: 'blue',
                        borderBottomLeftRadius: 12,
                        borderBottomRightRadius: 12
                    },
                    headerTintColor: 'white',
                    headerTitleStyle: { fontWeight: 'bold' }
                }}
            />
            <Stack.Screen
                name="EditNopol"
                component={EditNopol}
                options={{
                    title: 'Edit No. Polisi',
                    headerStyle: {
                        backgroundColor: 'blue',
                        borderBottomLeftRadius: 12,
                        borderBottomRightRadius: 12
                    },
                    headerTintColor: 'white',
                    headerTitleStyle: { fontWeight: 'bold' }
                }}
            />
        </Stack.Navigator>
    );
};

export default Router;
