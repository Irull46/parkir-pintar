import firebase from 'firebase';

firebase.initializeApp({
    apiKey: "AIzaSyAOVg3SQbeq42UKVgPBxQIL1lqb8EdN4Mc",
    authDomain: "parkir-pintar-efdde.firebaseapp.com",
    databaseURL: "https://parkir-pintar-efdde-default-rtdb.asia-southeast1.firebasedatabase.app",
    projectId: "parkir-pintar-efdde",
    storageBucket: "parkir-pintar-efdde.appspot.com",
    messagingSenderId: "4532332259",
    appId: "1:4532332259:web:0d42bd7cf48dd937d4c7d6"
});

const FIREBASE = firebase;

export default FIREBASE;