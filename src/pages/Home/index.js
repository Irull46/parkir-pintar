import React, { Component } from 'react';
import { Text, StyleSheet, View, TouchableOpacity, Alert, ScrollView, StatusBar } from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import FIREBASE from '../../config/FIREBASE';
import { CardParkir } from '../../components';

export default class Home extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             parkirs: {},
             parkirsKey: []
        }
    }

    componentDidMount() {
        this.ambilData();
    }

    ambilData = () => {
        FIREBASE.database()
            .ref("Parkir")
            .once('value', (querySnapshot) => {

                let data = querySnapshot.val() ? querySnapshot.val() : {};
                let parkirItem = {...data};

                this.setState({
                    parkirs: parkirItem,
                    parkirsKey: Object.keys(parkirItem)
                });

            });
    }

    removeData = (id) => {
        Alert.alert(
            "Perhatian",
            "Administrasi selesai, data akan dihapus!",
            [
              {
                text: "Cancel",
                onPress: () => console.log("Cancel Pressed"),
                style: "cancel"
              },
              { text: "OK", onPress: () => {
                  FIREBASE.database()
                    .ref('Parkir/'+id)
                    .remove();
                    this.ambilData();
                    Alert.alert('Berhasil', 'Penghapusan data selesai')
              } }
            ],
            { cancelable: false }
          );
    }
    
    render() {
        const { parkirs, parkirsKey } = this.state
        return (
            <View style={styles.page}>
                <StatusBar barStyle="light-content" backgroundColor="blue" />
                <ScrollView style={styles.listParkir}>
                    {parkirsKey.length > 0 ? (
                        parkirsKey.map((key) => (
                            <CardParkir key={key} parkirItem={parkirs[key]} id={key} {...this.props} removeData={this.removeData} />
                        ))
                    ) : (
                        <Text>Daftar kosong</Text>
                    )}
                </ScrollView>

                <View style={styles.wrapperButton}>
                    <TouchableOpacity
                    style={styles.btnTambah}
                    onPress={() => this.props.navigation.navigate('TambahNopol')}>
                        <FontAwesomeIcon icon={faPlus} size={20} color={'white'} />
                    </TouchableOpacity>
                </View>
            </View>
        );
    };
};

const styles = StyleSheet.create({
    page : {
        flex : 1,
        backgroundColor : 'white'
    },
    title : {
        fontSize : 20,
        fontWeight : 'bold',
        color : 'white'
    },
    listParkir : {
        paddingHorizontal : 30,
        marginTop : 15,
        marginBottom : 60
    },
    wrapperButton : {
        flex : 1,
        position : 'absolute',
        bottom : 0,
        right : 0,
        margin : 30
    },
    btnTambah : {
        padding : 20,
        backgroundColor : 'blue',
        borderRadius : 30,
        shadowColor : "#000",
        shadowOffset : { width : 0, height : 2 },
        shadowOpacity : 0.25,
        shadowRadius : 3.84,
        elevation: 5
    }
});
