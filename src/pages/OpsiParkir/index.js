import React, { Component } from 'react';
import { StyleSheet, View, TouchableOpacity, StatusBar } from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faCarSide, faMotorcycle, faTruck } from '@fortawesome/free-solid-svg-icons';

export default class OpsiParkir extends Component {
    render() {
        return (
            <View style={styles.page}>
                <StatusBar barStyle="dark-content" backgroundColor="white" />
                <TouchableOpacity
                    style={styles.opsiParkir}
                    onPress={() => this.props.navigation.navigate('Home')}>
                    <FontAwesomeIcon icon={faMotorcycle} size={55} color={'white'} />
                </TouchableOpacity>

                {/*opsi ini belum bisa dipakai karena masih menggunakan database yang sama dengan opsi parkir motor*/}
                <TouchableOpacity
                    style={styles.opsiParkir}
                    onPress={() => this.props.navigation.navigate('Home')}>
                    <FontAwesomeIcon icon={faCarSide} size={55} color={'white'} />
                </TouchableOpacity>

                {/*opsi ini belum bisa dipakai karena masih menggunakan database yang sama dengan opsi parkir motor*/}
                <TouchableOpacity
                    style={styles.opsiParkir}
                    onPress={() => this.props.navigation.navigate('Home')}>
                    <FontAwesomeIcon icon={faTruck} size={55} color={'white'} />
                </TouchableOpacity>
            </View>
        );
    };
};

const styles = StyleSheet.create({
    page : {
        flex : 1,
        alignItems : 'center',
        justifyContent : 'center',
        backgroundColor : 'white'
    },
    opsiParkir : {
        backgroundColor : 'blue',
        width : 90,
        height : 90,
        borderRadius : 10,
        alignItems : 'center',
        justifyContent : 'center',
        margin : 10,
        shadowColor : "#000",
        shadowOffset : { width : 0, height : 2 },
        shadowOpacity : 0.29,
        shadowRadius : 4.65,
        elevation: 7
    }
});
