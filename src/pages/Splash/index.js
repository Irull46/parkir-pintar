import React, {useEffect} from 'react';
import {Image, StyleSheet, View, StatusBar} from 'react-native';
import {Logo} from '../../assets';

const Splash = ({navigation}) => {
    useEffect(() => {
        setTimeout(() => {
            navigation.replace('OpsiParkir');
        }, 5000);
    });
    return (
        <View style={styles.wrapper}>
            <StatusBar barStyle="dark-content" backgroundColor="white" />
            <View style={styles.kotakLogo}>
                <Image source={Logo} style={styles.logo} />
            </View>
        </View>
    );
};

export default Splash;

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },
    kotakLogo : {
        backgroundColor : 'blue',
        paddingVertical : 20,
        paddingHorizontal : 25,
        borderRadius : 10
    },
    logo: {
        width : 50,
        height : 60
    }
});