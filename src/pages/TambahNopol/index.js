import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, View, Text, Alert, StatusBar } from 'react-native';
import { InputData } from '../../components';
import FIREBASE from '../../config/FIREBASE';

export default class TambahNopol extends Component {
    constructor(props) {
        super(props);
    
        this.state = {
             nama: '',
             nopol: '',
        };
    }

    onChangeText = (namaState, value) => {
        this.setState({
            [namaState] : value
        });
    };

    onSubmit = () => {
        if(this.state.nama && this.state.nopol) {
            const parkirReferensi = FIREBASE.database().ref('Parkir');
            const parkir = {
                nama: this.state.nama,
                nopol: this.state.nopol
            }
            
            parkirReferensi
                .push(parkir)
                .then((data) => {
                    Alert.alert('Berhasil', 'Data telah ditambahkan');
                    this.props.navigation.replace('Home');
                })
                .catch((error) => {
                    console.log("Error : ", error);
                })

        }else {
            Alert.alert('Gagal', 'Nama dan No. Polisi harus diisi!');
        }
    };
    

    render() {
        return (
            <View style={styles.pages}>
                <StatusBar barStyle="light-content" backgroundColor="blue" />
                <InputData
                    label="Nama"
                    placeholder="Masukkan Nama"
                    onChangeText={this.onChangeText}
                    value={this.state.nama}
                    namaState="nama"
                />
                
                <InputData
                    label="No. Polisi"
                    placeholder="Masukkan No. Polisi"
                    onChangeText={this.onChangeText}
                    value={this.state.nopol}
                    namaState="nopol"
                />

                <TouchableOpacity style={styles.tombol} onPress={() => this.onSubmit()}>
                    <Text style={styles.textTombol}>SUBMIT</Text>
                </TouchableOpacity>
            </View>
        );
    };
};

const styles = StyleSheet.create({
    pages : {
        flex : 1,
        padding : 30,
        backgroundColor : 'white'
    },
    tombol : {
        backgroundColor : 'blue',
        padding : 15,
        borderRadius : 10,
        marginTop : 14,
        shadowColor : "#000",
        shadowOffset : { width : 0, height : 2 },
        shadowOpacity : 0.25,
        shadowRadius : 3.84,
        elevation: 5
    },
    textTombol : {
        color : 'white',
        fontWeight : 'bold',
        textAlign : 'center',
        fontSize : 16
    }
});
