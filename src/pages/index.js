import Splash from './Splash';
import OpsiParkir from './OpsiParkir';
import Home from './Home';
import TambahNopol from './TambahNopol';
import EditNopol from './EditNopol';

export { Splash, OpsiParkir, Home, TambahNopol, EditNopol };