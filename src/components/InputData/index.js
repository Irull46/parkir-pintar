import React from 'react';
import { StyleSheet, Text, TextInput } from 'react-native';

const InputData = ({label, placeholder, onChangeText, namaState, value}) => {
    return (
        <>
            <Text style={styles.nopol}>{label} :</Text>
            <TextInput
                placeholder={placeholder}
                placeholderTextColor={'silver'}
                style={styles.textInput}
                value={value}
                onChangeText={(text) => onChangeText(namaState, text)}
            />
        </>
    );
};

export default InputData;

const styles = StyleSheet.create({
    nopol : {
        fontSize : 16,
        paddingBottom : 4
    },
    textInput : {
        borderRadius : 10,
        padding : 10,
        color : 'black',
        backgroundColor : 'white',
        marginBottom : 10,
        shadowColor : "#000",
        shadowOffset : { width : 0, height : 2 },
        shadowOpacity : 0.25,
        shadowRadius : 3.84,
        elevation: 5
    }
});
