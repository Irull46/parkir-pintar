import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { faCheck, faEdit } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';

const CardParkir = ({id, parkirItem, removeData, navigation}) => {
    return (
        <TouchableOpacity style={styles.container}>
            <View>
                <Text style={styles.nama}>{parkirItem.nama}</Text>
                <Text style={styles.nopol}>No. Polisi : {parkirItem.nopol}</Text>
            </View>
            <View style={styles.icon}>
                <FontAwesomeIcon icon={faEdit} color={'blue'} size={22} marginRight={7} onPress={() => navigation.navigate('EditNopol', {id: id})} />
                <FontAwesomeIcon icon={faCheck} color={'green'} size={22} onPress={() => removeData(id)} />
            </View>
        </TouchableOpacity>
    )
}

export default CardParkir;

const styles = StyleSheet.create({
    container: {
        flexDirection : 'row',
        padding : 15,
        backgroundColor : 'white',
        borderRadius : 10,
        marginBottom : 20,
        shadowColor : "#000",
        shadowOffset : { width : 0, height : 2 },
        shadowOpacity : 0.25,
        shadowRadius : 3.84,
        elevation: 5
    },
    nama : {
        fontWeight : 'bold',
        fontSize : 16
    },
    nopol : {
        fontSize : 12,
        color : 'gray'
    },
    icon : {
        flexDirection : 'row',
        flex : 1,
        justifyContent : 'flex-end',
        alignItems : 'center',
    }
});
